package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import cucumber.api.java.en.When
import groovy.swing.impl.DefaultAction
import internal.GlobalVariable
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import stepDefinition.Login

public class Add_Product {
	@When("user click on the Jual button")
	public void user_click_on_the_Jual_button() {
		WebUI.click(findTestObject('Object Repository/Add Product/btn-jual'))
	}
	@When("user view the display of Add Product page")
	public void user_view_the_display_of_Add_Product_page() {
		//Wait element button publish appear to validate that user is in Add Product Page
		WebUI.waitForElementPresent(findTestObject('Object Repository/Add Product/btn-publish'), 0)
	}
	@When("user fill all the required fields")
	public void user_fill_all_the_required_fields() {
		//After that, find element product name & input product name field
		WebUI.setText(findTestObject('Object Repository/Add Product/input-product-name'), 'Honda Vario Rangka eSAF 2021')
		//Find element product price & input product price field
		WebUI.setText(findTestObject('Object Repository/Add Product/input-product-price'), '1000000')
		//Find element dropdown category and choose number 2 of array
		WebUI.selectOptionByIndex(findTestObject('Add Product/input-category'), 2)
		//Find element description & input description field
		WebUI.setText(findTestObject('Object Repository/Add Product/input-description'), 'Ngondah Semakin Terxixiixix')
		//Wait element input photo appear
		WebUI.waitForElementPresent(findTestObject('Object Repository/Add Product/input-photos'), 0)
		//Verify element input photos
		WebUI.verifyElementPresent(findTestObject('Object Repository/Add Product/input-photos'), 0)
		//After element input photos has been verified, Upload JPG File
		WebUI.uploadFile(findTestObject('Object Repository/Add Product/input-photos'), 'D:\\Downloads\\Patrick2.jpg')
	}
	@When("user click the Terbitkan button")
	public void user_click_the_Terbitkan_button() {
		//JPG file has been inputted, click button publish
		WebUI.click(findTestObject('Object Repository/Add Product/btn-publish'))
	}
	@Then("user successfully add product and redirected to Product page")
	public void user_successfully_add_product_and_redirected_to_Product_page() {
		//Wait element button delete appear to validate that user is in Product page
		WebUI.waitForElementPresent(findTestObject('Object Repository/Add Product/btn-delete'), 0)
	}
}