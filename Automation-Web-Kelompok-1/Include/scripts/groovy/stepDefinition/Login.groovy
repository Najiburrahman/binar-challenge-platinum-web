package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import java.security.PublicKey

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

public class Login {
	@Given("user is in Login page")
	public void user_is_in_login_page() {
		WebUI.openBrowser('https://secondhand.binaracademy.org/users/sign_in')
		WebUI.verifyElementPresent(findTestObject('Object Repository/Login/input-email'), 0)
		WebUI.verifyElementPresent(findTestObject('Object Repository/Login/input-password'), 0)
		WebUI.verifyElementPresent(findTestObject('Object Repository/Login/btn-login'), 0)
	}
	@When("user input valid credentials and click login button")
	public void user_input_valid_credentials_and_click_login_button() {
		WebUI.getAttribute(findTestObject('Object Repository/Login/input-email'), 'validationMessage')
		WebUI.setText(findTestObject('Object Repository/Login/input-email'), 'raerifai@gmail.com')
		WebUI.setText(findTestObject('Object Repository/Login/input-password'), 'Asd123456!')
		WebUI.click(findTestObject('Object Repository/Login/btn-login'))
	}
	@Then("user can successfully login")
	public void user_can_successfully_login() {
		WebUI.verifyElementPresent(findTestObject('Object Repository/Login/icon-has-been-login'), 0)
		//WebUI.closeBrowser()
	}
}
