Feature: Login

  Scenario: User can Login
    Given user is in Login page
    When user input valid credentials and click login button
    Then user can successfully login
