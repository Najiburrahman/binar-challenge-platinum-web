Feature: Product Detail

  Scenario: User views product based on category
    Given user is in the Homepage
    When user check category section
    And user click one of the Category button
    Then page will be listed products based on selected category

  Scenario: User searches for a product
    Given user is in the Homepage
    When user click on Search field
    And user input product name
    And user press Enter
    Then page will be listed products based on keyword

  Scenario: User bargains a product without login
    Given user is in the Homepage
    When user view the product section
    And user select one of the product
    And user click Saya Tertarik dan Ingin Nego button
    And user input Harga Tawar field
    And user click Kirim button
    Then user will be redirected to the Login page

  Scenario: User bargains a product without completing user profile
    Given user has login and view the Homepage
    When user view the product section
    And user select one of the product
    And user click Saya Tertarik dan Ingin Nego button
    And user input Harga Tawar field
    And user click Kirim button
    Then user will be redirected to the User Profile page

  Scenario: User bargains a product with valid data
    Given user has login and view the Homepage
    When user view the product section
    And user select one of the product
    And user click Saya Tertarik dan Ingin Nego button
    And user input Harga Tawar field
    And user click Kirim button
    Then confirmation message will be displayed