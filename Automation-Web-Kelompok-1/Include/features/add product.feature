Feature: Add Product

  Scenario: User can Login
    Given user is in Login page
    When user input valid credentials and click login button
    Then user can successfully login
    And user click on the Jual button
    And user view the display of Add Product page
    And user fill all the required fields
    And user click the Terbitkan button
    Then user successfully add product and redirected to Product page
